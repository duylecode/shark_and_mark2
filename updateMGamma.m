function [ new_gamma ] = updateMGamma( M_record, alphas )
% the mark decides whether or not to accept
%function discarded for now

a = sum(M_record == 1)/size(M_record,2); % times mark has won?
b = mean(alphas);

new_gamma = ((b * a) / (a*b + (1-a)*b));
end


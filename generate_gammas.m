function [ out_gammas ] = generate_gammas( gammas )



for i = 1:1:size(gammas,2)
    r = rand();
if(r > 0.5)
    r = 1;
else
    r = 0;
end
    gammas(i) = r;
end

out_gammas = gammas;
end


function [ new_skill ] = update_estimates(M_skill, M_S_skill)

%M_S_skill = bayes(p_win, old M_s_skill)
%we assume with stochasticity 
if(M_skill >= M_S_skill)
    a = 0.8;
else
    a = 0.2;
end

b = M_S_skill;


% p(skill ,  win)
% a = win
% b = skill
new_skill = (b * a) / ((a * b) + (1-a)*(1-b));

end


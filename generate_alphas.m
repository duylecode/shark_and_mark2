function [ out_alphas ] = generate_alphas( alphas )

for i = 1:1:size(alphas,2)
    r = randi([0, 10]);

    alphas(i) = r;
end

out_alphas = alphas;
end




%alphas = input('insert aphas\n'); % wagers
% S = shark = scamer; m = mark = scamee

%gammas = input('intput gammas\n'); % win/loss policy (start w/ 0s and 1s)
gammas = [1,0,0,1,0,0,0,0,1,0,0,0,0,0,1];
gammas = generate_gammas(gammas);
alphas = [1,0,0,10,0,0,0,0,10,0,0,0,0,0,10];
alphas = generate_alphas(alphas);
k = size(gammas,2); % we assume s alphas == s gammas == k

alphas_ = zeros(1,k); % for record?
if(size(alphas) ~= size(gammas)) % see below
    disp('Your vectors need to be the same length');
    return;
end

M_skill = 0.2 + (.6)*rand();
M_S_skill = M_skill;
S_budget = 100;
S_pocket = S_budget; % how much he has to spend
M_budget = 100000000; % M_budget is now infinite
M_loss_prc = 0.5; % unused
M_rho = 10; % fixed, no small wins

S_M_rho = M_rho; %we assume shark knows exactly how much he can poach
beta = 1; % we assume the mark will accept the first game

S_M_skill = M_skill; % the shark knows his victim's skill

M_record = zeros(1,k); % mark remembers in -1, 1
S_profit = zeros(1,k);
betas_ = zeros(1,k); %yes the underscore means record for data tracking
M_S_skills_ = zeros(1,k);

for t = 1:k
    
   alpha = alphas(t);
   alphas_(t) = alpha / M_rho; % records wagers as a prc of mark pocket
   
   if(alpha > S_pocket) %making sure the alphas are reasonable
       disp('wager exceeded sharks budget');
       return;
   end
   if(alpha > M_rho)
       disp('wager exceeded marks budget');
       return;
   end
   
   if(M_rho <= 0 || S_pocket <= 0)
       disp('someone went broke');
       return;
   end
   
   if(alpha > M_rho)
    beta = 0;% 2 for temporary
   elseif(think(M_S_skill, alpha) > M_rho) % alpha(1-theta) > rho
    beta = 0;
   else
    beta = 1;
   end
   
   betas_(t) = beta;
   
   if(beta == 1)
    S_gamma = gammas(t);
    r = rand();
    if(r < 0.2) % adding stochasticity to the program
        %S_gamma = S_gamma -1; temp. removing
    end
    if(S_gamma >= M_skill) % things are getting weird
      M_budget = M_budget - alpha;
      S_pocket = S_pocket + alpha;
      M_budget = M_budget - alpha;
      S_budget = S_budget + alpha;
      S_profit(t) = alpha;
      M_record(t) = -1;
    else
       M_budget = M_budget + alpha;
       S_pocket = S_pocket - alpha;
       S_profit(t) = -alpha;
       M_budget = M_budget + alpha;
       S_budget = S_budget - alpha;
       M_record(t) = 1;
    end
   end

        M_S_skill = update_estimates(M_skill,M_S_skill); % the updating you want
        M_S_skills_(t) = M_S_skill;
    
    M_rho = M_loss_prc * M_budget;
end
%plotting
profit_average = zeros(1, k);
sum = 0;
for x = 1:k
    sum = sum + S_profit(x);
    profit_average(x) = sum / x;
end

subplot(2,2,1);

plot(1:k, gammas);
hold on;
plot(1:k, S_profit);
plot(1:k, profit_average);
hold off;
legend('policy', 'profit', 'average profit');
title('Profit and policy vs time');

subplot(2,2,2);

plot(1:k, M_S_skills_);
hold on;
plot(1:k, gammas);
hold off;
legend('M_S_Skill', 'policy');
title('Policy and M\_S\_skill vs time');

subplot(2,2,3);

plot(1:k, alphas);
hold on;
plot(1:k, gammas);
hold off;
legend('Wagers', 'Policy');
title('Wager and Policy vs time');